const mysql = require('mysql');

const HOST = 'localhost';
const USER = 'root';
const PASSWORD = 'toor';
const DB_NAME = 'TBserviceCodes';
const PORT = 3306;

const connection = mysql.createConnection({
    host: HOST,
    user: USER,
    password: PASSWORD,
    database: DB_NAME,
    port: PORT
});

const connectDB = async () => {
    await connection.connect((error) => {
        if (error) {
            console.error('Connection error:', error);
            return;
        }
        console.log('DB connected successfully!');
    });
}

const disconnectDB = () => {
    return new Promise((resolve, reject) => {
        connection.end((err) => {
            if (err) {
                console.error('[ERROR] Error while closing connection:', err);
                reject(err);
            } else {
                console.log('[OK] Conection closed successfully!');
                resolve();
            }
        })
    });
}

const executeQuery = (query) => {
    return new Promise((resolve, reject) => {
        connection.query(query, (error, results) => {
            if (error) {
                console.error('[ERROR] Error executing query:', error);
                reject(error);
            } else {
                console.log('[OK] Query executed succesfully!');
                resolve(results);
            }
        });
    });
}

const executeQueryWithObj = (query, obj) => {
    return new Promise((resolve, reject) => {
        connection.query(query, obj, (error, results) => {
            if (error) {
                console.error('[ERROR] Error executing query:', error);
                reject(error);
            } else {
                resolve(results);
            }
        });
    });
}

module.exports = {
    connection,
    connectDB,
    disconnectDB,
    executeQuery,
    executeQueryWithObj
}