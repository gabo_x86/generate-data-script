const { connectDB, disconnectDB, executeQuery } = require('./config.js');

(async () => {
    const CPT = 97110;
    const SC_ID = 45873;
    const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    let query = ``;

    await connectDB();

    for (let i = 0; i < letters.length; i++) {
        for (let j = 0; j < letters.length; j++) {
            query = `INSERT INTO zzz_tbdemo(sc_id, description, code, modifier) 
                VALUES (${SC_ID}, 'TEST', ${CPT}, '${letters[i]}${letters[j]}');`;
            executeQuery(query);
        }
    }
    await disconnectDB();
})();

