const { connectDB, disconnectDB, executeQuery, executeQueryWithObj } = require('./config.js');

/**
 * @param {number} eobid
 * @param {number} epeater
 * @returns {void}
 */
( async () => {
    const eobid = process.argv[2];
    const repeater = process.argv[3];

    connectDB();
    if (!process.argv[2]) {
        console.log('Please provide an EOBID param');
    } else {
        const selectQuery = `SELECT * FROM TB835.tbdemo WHERE EOBID=${eobid}`;
        const results = await executeQuery(selectQuery);
        if (results.length === 0) {
            console.error(`[ERROR] The EOBID ${eobid} doesn\'t exist in TB835.tdemo table`);
        } else {
            let insertQuery = 'INSERT INTO TB835.tbdemo SET ?';
            const newResults = results.map(e => {
                delete e.id;
                return e;
            });
            for(let i=0; i<repeater ;i++){
                newResults.forEach(async (element) => {
                   console.log(`Session: ${element.avSessID} / EOBID: ${element.EOBID} (${i+1}) was cloned!`);
                   await executeQueryWithObj(insertQuery, element);
                });
            }
            
        }
    }
    await disconnectDB();
})();

